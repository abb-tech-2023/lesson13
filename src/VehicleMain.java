package src;

public class VehicleMain {

    public static void main(String[] args) {
        Car car = new Car();
        car.startEngine();
        car.drive();

        Motorcycle motorcycle = new Motorcycle();
        motorcycle.startEngine();
        motorcycle.drive();

        Truck truck = new Truck();
        truck.startEngine();
        truck.drive();
    }
}
