package src;

public class Truck extends  Vehicle{
    @Override
    public void startEngine() {
      System.out.println("Truck startEngine()");
    }

    @Override
    public void drive() {
        System.out.println(  " Truck drive()");
    }
}
