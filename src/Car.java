package src;

public class Car extends Vehicle{

    @Override
    public void startEngine() {
        System.out.println("Car startEngine()");
    }

    @Override
    public void drive() {
        System.out.println(  "Car drive()");
    }
}
