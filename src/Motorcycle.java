package src;

public class Motorcycle extends  Vehicle {
    @Override
    public void startEngine() {
        System.out.println("Motorcycle startEngine()");
    }

    @Override
    public void drive() {
        System.out.println(  "Motorcycle drive()");
    }
}
